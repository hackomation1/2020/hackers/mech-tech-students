# 4. Business & Marketing

## 4.1 Objectives

Problem: Safe and good local food is a must for tourism. It is a challenge to provide mercury free fish and fresh produce for tourists while they enjoy the spectacular nature in the interior of Suriname.

Solution: For the problem above, we built a semi-automated IOT system(Autoponics system). This sytem povides the ability to remotely monitor hydroponic system parameters while automatically regulating the pH of water.

Steps taken:
1. First we made the framework

2. Then we built the sensors(pH, temperature and growlight sensor)

3. We also tested the sensors individually

4. Then we built the sensors on arduino Esp32 and linked them to the IOT platform

5. After that we attached the sensors to the framework

6. Finally we create a business model for the project

## 4.2 Ananlyze your market & segments
The target market
1.	Tourist Lodges in the Interior of Suriname
2.	Hydroponics and aquaponics farms in Suriname
3.	Agriculture schools (more advanced systems)

Value proposition: You can produce pesticide free vegtables and mercury free fish at home. You do not have to leave home for fresh food. Stay home = Stay save

We will deal our product in two different packages. The first package will consist of only the IOT system. The second package will consist of the IOT system and the hydroponics or aquaponics setup. The costs of the product are based on the size and the type of setup and the sensor package an location. The cost of our second (basic) package starts at $180. This package consists of three sensors (the temperature sensor, pH sensor and a light dependent sensor) and the hydroponics/aquaponics setup.

## 4.3 Build your business canvas


![](https://snipboard.io/ETeC7q.jpg)
## 4.4 Build your pitch deck

[Link to pitch](https://docs.google.com/presentation/d/1civzsCS-ScTX5jXzgF62JrSC8oz9pGGU1q0xcgJSpGc/edit?usp=sharing)

## 4.5 Make a project Poster
![](https://snipboard.io/rU8I7R.jpg)

## 4.6 Files & Code
[Click here for the code](https://gitlab.com/hackomation1/2020/hackers/mech-tech-students/-/blob/e6ab7f824910ef80927f828051f047b24a529ac5/docs/files/Final_code_1_.zip)


## 4.7 References & Credits
[humidity](https://wiki.dfrobot.com/DHT11_Temperature_and_Humidity_Sensor_SKU_DFR006
7_-DFRobot)

[temperature sensor](https://wiki.dfrobot.com/Waterproof_DS18B20_Digital_Temperature_Sensor_SKU_
DFR0198_-DFRobot)

[pH sensor](https://github.com/Greenponik/DFRobot_ESP_PH_BY_GREENPONIK)

[light dependent sensor](https://somtips.com/automatic-night-light-using-Ldr-and-relay-in-esp8266/)

[flowrate sensor](https://maker.pro/arduino/tutorial/how-to-interface-arduino-with-flow-rate-sensor-tomeasure-liquid)

[buzzer](https://create.arduino.cc/projecthub/SURYATEJA/use-a-buzzer-module-piezospeaker-using-arduino-uno-89df45)

[using blynk](https://maker.pro/arduino/projects/how-to-remotely-control-an-arduino-with-the-blynk-app#:text=according%20to20Blynk's%20website%2C%20%E2%80%9CBlynk,simply%20dragging%20and%20dropping%20widgets.22>)
