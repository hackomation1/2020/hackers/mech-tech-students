## AUTOPONICS
![](https://snipboard.io/rezm1h.jpg)


## Description

Autoponics is a semi-automated IOT system, which is applicable to both hydroponics and 
aquaponics systems. This system consists of three sensors and a microprocessor (the number 
and type of sensors can be increased/ changed according to the needs of the client). 

## Overview & Features

- The autoponics IOT system gives the client the ability to check the pH, light intensity and water temperature parameter values at any moment of the day. Due to the automatic notification systems, an aquaponics or hydroponics farm which uses the autoponics installation requires less attention than conventional hydroponics or aquaponics farms.The farming process is also easier to monitor. 

- The parameter values that the sensors measure can be monitored from anywhere with acces to Wif-Fi and the Blynk app. With this feature, the farmer does not have to be present at location to measure important system parameters.

-  The built in automatic pH feedback system can reduce death of fish and plants caused by fluctuating pH in aquaponics systems. Our product, autoponics, regulates the pH when it crosses a certain value(enters the danger zone). With this feature we adressed and solved one of the major problems in aquaponics systems: fish and plant death due to low pH values. 

- Autoponics can be modified to run on solar panels/batteries (using small turbines for example). If there is no internet connection, IOT system can be switched of , thereby converting the system into conventional hydro or aquaponic farming.

- Autoponics is aquaponics based. Because with aquaponics, one can grow edible plants, fruits and several types of fish species. For example lettuce, bell pepper, watermelons, tomatoes, tilapia etc. A wide variety of produce and fish, benefitting the farmers and tourist lodges.


## Demo / Proof of work

Proof of concept working (check videos below)

 - [demo 1](https://www.youtube.com/watch?v=s3YoTxTuGjs)
 - [demo 2](https://www.youtube.com/watch?v=zT3LBj6oLos)

## Deliverables

- Product pitch deck [View slides here](https://docs.google.com/presentation/d/1civzsCS-ScTX5jXzgF62JrSC8oz9pGGU1q0xcgJSpGc/edit?usp=sharing)

## Project landing page/website (optional)

 This is our youtube channel [click here](https://www.youtube.com/channel/UCc95tMcgwckLKZjase6u8KA).
 Don't forget to like and subscribe.

 Go follow or facebook page [click here](https://www.facebook.com/Mech-tech-students-105673651396862/).
## The team & contact

**A group of Mechanical engineering students at Anton de Kom University of Suriname.**

![](https://snipboard.io/EmVYtU.jpg)

- Bisessar Wireshkoemar (bisessarw@gmail.com)

![](https://snipboard.io/GPC1NT.jpg)

- Chotkan Nawin (chotkan3@gmail.com)

![](https://snipboard.io/4bI7gD.jpg)

- Ho Argiel (saphierho@gmail.com)

![](https://snipboard.io/wrYuX2.jpg)

- Phagoe Kaajol (Kaajol_98@live.com)

![](https://snipboard.io/CSFfEP.jpg)

- Zheng Andy (cskzheng@gmail.com)


