# 2. Embedded solution
We wanted to create a semi-automated aquaponics system using different types of sensors and a microprocessor. Autoponics is aquaponics based. Autoponics, an IOT system that provides the ability to remotely monitor hydroponic system parameters while automatically regulating the PH of the water.
## 2.1 Objectives
Problem: We had to choose sensors and a microprocessor (which can connect to the internet) for our system.

Solution: We chose 6 sensors(during the design phase) namely pH sensor,temperature sensor, the light dependent sensor, water flow sensor, humidity sensor and ultrasonic sensor. As our microprocessor we chose ESP 32 TTGO module. 


## 2.3 Steps taken

-  We first searched for information about aquaponics (if you cover aquaponics, then you already know how to do hydroponics). 

-  Then we looked at how we exactly wanted our design (framework etc.)  

-  We then checked which materials and components are needed and then made a list of them 

-  Then we made a cost calculation of the design 

For the design of this project, we divided the project into two parts namely:

- The farming part (constructing and running the hydroponic set-up and using this system to grow the lettuce). 
- IOT part (setting up all the sensors, writing the codes etc) 

**Steps taken in the perform phase:**

- We first started with the framework. In the picture below you can see the frame and components of the hydroponics part.
![](https://snipboard.io/Aj5axc.jpg)


- We had already grown lettuce seedlings (see picture below).
![](https://snipboard.io/C9yt1p.jpg)

- After the construction was finished, the plants were put in.

- We then started looking up programming codes for the chosen sensors with their libraries.

 ![](https://snipboard.io/9SxkNv.jpg)

- After this, a plan was made for the output of the IoT portion.
- Then we built and tested the sensors.
  ![](https://snipboard.io/WspT38.jpg)

- After all the sensors have been tested, we have assembled them all and placed them in a cointainer (see picture below).

 ![](https://snipboard.io/xrUMY7.jpg)

- Finally, we attached that container to the frame (the plants had grown by then). The picture below illustrates the complete system.

 ![](https://snipboard.io/QKGZMS.jpg)


## 2.4 Testing & Problems
**Problems in designing our framework** 
We had to choose between a wooden frame and a metal frame.
We choose a wooden frame, because a wooden frame is much cheaper than a metal frame and it is also strong enough to support the weight of the PVC pipes with water and plants. Also simply because the wood was already available.

**Problems with the IOT parts**
It was necessary to check which variables needed to be measured to choose the sensors. In other words it was necessary to identify which variables are most important to the growth of the plants in this type of farming.
Chosen variables:

•	Water temperature.

•	Acidity(pH) of the water

•	Humidity of the environment.

•	Flow of water that goes to the pipes in which the plants sit. 

•	Light intensity on the plants.

•	Height of the plants.	

Chosen sensors during the design phase:

1. Temperature sensor.

2. Acidity (pH) sensor.

3. Humidity sensor.

4. Water flow sensor.

5. Light dependent sensor.

6. Ultrasonic sensor.

With the following functions:

1.	The temperature sensor will measure temperature of water in the water container (keeping the water in the optimal temperature range enhances the fish growth and health, different species require different temperature ranges).

2.	The acidity (pH) sensor will measure the pH-value of the water that will circulate through the system (in aquaponics, the irregular accumulation of fish waste can cause a sudden decrease in pH, and if this is not corrected on time, all the crops and fish can die).

3.	Humidity sensor will measure the humidity of the surrounding air.

4.	Flow sensor will measure the volume flow of the water that circulates through the pipes in which the plants sit.

5.	Light dependent sensor will measure the light intensity on the plants.

6.	Plant height ultrasonic will measure the height of the plants.


**Problem after cost calculation**

The total cost for the IOT components is $ 458.34. The total cost is much higher than the maximum amount ($ 250). 
Solution: To reduce costs for the IOT components, ultrasonic sensor is first removed from the list. Secondly, the components for feeding the fish are removed from the list. As a result, we will no longer use fish. The costs for the materials used for framework are also paid out of our own pocket.

IMPORTANT: Autoponics is an iot system that is applicable to hydroponics, aquaponics and aquaculture systems. it is based on aquaponics, which includes both plants and fishes. but because of the budget restrictions and the lack of knowledge regarding the farming of fish, we chose to grow only lettuce (hydroponics), but the iot part of the system (which forms the core of the product) is perfectly applicable to systems with fish and plants or only fish.

**Problems with the sensors**

- The pH value measured by the pH sensor did not remain constant (it started to fluctuate). When the pH probe is placed in a liquid, the probe measures a value, which is converted into voltage. This voltage is converted into the pH value by means of a formula in the program. When the voltage started to fluctuate, the pH value did not remain constant.

- The pH sensor had to be recalibrated over and over again.

- The pH sensor was slow to respond. When the pH probe was placed in an acidic solution, the program did indicate the correct value. However, when the probe was subsequently placed in a basic solution, it took time to get to the correct pH value (value associated with the basic solution). The response time of the pH sensor was slow. This caused a problem for turning the backup waterpump on and off. The waterpump turns automatically on at a low pH value (pH value<5.5).Because the response time of the pH sensor  was slow when switching  from a low pH value to a high pH value, slightly more water was pumped into the system.

- The flow sensor did not respond. As a result, we had to remove that flow sensor from our system.

- Due to problems with the pH sensor, we omitted the humidity sensor due to lack of time. As a result, we have focused more on the pH sensor, the light dependent sensor and the temperature sensor.

- The TTGO ESP 32 board's USB connector was weak. While creating a suitable enclosure for the ESP 32 board, the USB cable had to be disconnected again, causing the USB connector to become loose. After this, we carefully glued it so that it could function properly again. It worked!

## 2.5 Proof of work


![](https://snipboard.io/wnCQbI.jpg)
![](https://snipboard.io/nGYj8Z.jpg)
![](https://snipboard.io/U9Glku.jpg)


## 2.6 Files & Code

 [Link to code](https://gitlab.com/hackomation1/2020/hackers/mech-tech-students/-/blob/e6ab7f824910ef80927f828051f047b24a529ac5/docs/files/Final_code_1_.zip)
 

## 2.7 References & Credits


- [Humidity sensor](https://wiki.dfrobot.com/DHT11_Temperature_and_Humidity_Sensor_SKU_DFR
0067_-DFRobot)
- [Temperature sensor](https://wiki.dfrobot.com/Waterproof_DS18B20_Digital_Temperature_Sensor_SK
U_DFR0198_-DFRobot)
- [pH sensor](https://github.com/Greenponik/DFRobot_ESP_PH_BY_GREENPONIK)
- [light dependent sensor](https://somtips.com/automatic-night-light-using-Ldr-and-relay-in-esp8266/)
- [Flow sensor](https://maker.pro/arduino/tutorial/how-to-interface-arduino-with-flow-rate-sensorto-measure-liquid)
- [Buzzer](https://create.arduino.cc/projecthub/SURYATEJA/use-a-buzzer-module-piezospeaker-using-arduino-uno-89df45)
