# 5. Blog 

## ==[`2020-08-16`]==
![](https://snipboard.io/PrVOIc.jpg)
![](https://snipboard.io/VGpOj5.jpg)

After the idea pitching, we went on to work on the hydroponics part. We first sketched an easy to build model. We build it using wood(which we already had at our exposure). The wooden frame was done. The next step was to paint the frame, which we did with paint that we bought.

The following part was installing the pipe system. All the components were listed and bought afterwards. First there had to be holes made into the pipes that would carry the plants, afterwards, everything was fit and glued together to make the finished(hydroponics part) system.

## ==[`2020-10-09`]==
![](https://snipboard.io/2XFLMO.jpg)

The lettuce seedlings were sowed. After some weeks they were ready to be put into the hydroponic system that we build earlier.

## ==[`2020-10-10`]==

The seedlings were put into the system. The fertilizers were added to the feedwater, and the TDS and pH value were checked daily. The ideal pH range for this lettuce species is 5.5-6.5, hence we had to keep it in this range. The TDS value had to be around 800 ppm, when it dropped, we added more fertilizer.

## ==[`2020-10-15`]==
![](https://snipboard.io/eoyK6N.jpg)

We also went to the IOT-lab for soldering. Soldering was completed successfully. Then we built and tested the pH sensor with the programming code. The program indicate values, but they were not correct. Then we calibrated the pH sensor. We also started building our the temperature sensor and tested it with the programming code. The temperature sensor indicate the correct values. On the first day in IOT- lab, we spent an average time of 4 hours.

## ==[`2020-10-21`]==

On  21  october 2020 we went to the IOT-lab again. On this day we built and tested the LDR sensor with the programming code. Building and testing the programming code was successful. After this we built all sensors on Arduino ESP32 and tested with te programming code. This was also succesful. Finally, we linked the sensors to Blynk and tested if we could read the values on our mobile. Finally, this also ended successfully.

## ==[`2020-11-10`]==

The lettuce took almost 6 weeks to become harvest ready, and this would be in the middle of November. But the finals were postponed to the 21 of November, and the lettuce would not have made it. We took new lettuce plants from another system of ours, these were mid-sized. But we could not grow them in the sytem we build, because we had to leave it at the lab, because of some technical IOT issues which we had discovered in the first week of November. We had almost two weeks to build a new hydroponics system(onlyto grow the plants). We build a system that does not need electricity or pumps, with some wood, plastic and PVC sheets. The new plants were added to this system. They grew into harvest ready plants, which we presented at the hackomation finales on the 29 of November.

## ==[`2020-11-11`]==
![](https://snipboard.io/o2pYcN.jpg)
![](https://snipboard.io/a46qde.jpg)

On 11 November 2020 we went to the Wb-lab first to attach all sensors on the framework. We also attached the growlight on the framework. When compliling the programming code it turned out that the Wifi connection was slow. After this, we moved our framework the IOT-lab.

## ==[`2020-11-19`]==
On 19 November 2020 we went the the IOT-lab again to test the code with Wifi and mobile data. Testing was successful. After this, we made a video of how the system work.

## ==[`2020-11-27`]==
On       November we went back to the IOT- lab to clean the the framework. After this we started testing the code again to see if our project was ready for the pitching. Testing  was successful. Finally, we pasted our project logo on the framework.

## ==[`2020-11-29`]==
![](https://snipboard.io/NBVEY2.jpg)
![](https://snipboard.io/CIDMqK.jpg)

**PITCH DAY**
On this day we were on time at Lalla Rookh. We were a little bit nervous, but also ready for the pitching. The pitching went well. Our project worked perfect! The jury asked us interesting questions. This event was perfect!




