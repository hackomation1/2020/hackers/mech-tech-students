# 3. Front-End & Platform
We used 3 senors in our system namely the pH sensor, light dependent sensor and water temperature sensor.
Using all these sensors, wifi and an IOT platform our system can be monitored.

## 3.1 Objectives
Problem: We had to search for an IOT platform that would dislpay the pH value, the LDR value and the temperature of water. We also wanted to get notifications and emails, when the pH value is in a unsafe region and when the growled is turned on and off.

Solution: blynk, which supports hardware platforms such as Arduino, esp 32, Raspberry Pi, and similar microcontroller boards to build hardware for your projects.


## 3.3 Steps taken

Steps taken:
- we first downloded the app "blynk" via browser/ Playstore

![](https://snipboard.io/wv8AFV.jpg)

- Then we created an account on blynk

![](https://snipboard.io/Ba8POV.jpg)

- We then created a new project on blynk

![](https://snipboard.io/XPk91v.jpg)

- We then started adding controllers on the blynk dashboard

- We first added a LCD dislpay to display the pH value and the water temperature 

![](https://snipboard.io/DOfycv.jpg)

- Then we added a superchart to display the pH graph

![](https://snipboard.io/HlpKNF.jpg)

- Then we added the notification setting, the email setting and a value display

![](https://snipboard.io/ruTOpL.jpg)
![](https://snipboard.io/TCl4De.jpg)
![](https://snipboard.io/aEreWn.jpg)

- At the end we connected it to our system by using its " Auth Token"

## 3.4 Testing & Problems
Problems with the execution of the project
1.	The pH value measured by the pH sensor did not remain constant (it started to fluctuate).

2.	When the pH probe is placed in a liquid, the probe measures a value, which is converted into voltage (voltage). This voltage is converted into the pH value by means of a formula in the program. When the voltage started to fluctuate, the pH value did not remain constant.

3.	The pH sensor had to be recalibrated over and over again.

4.	The pH sensor was slow to respond.

5.	When the pH probe was placed in an acidic solution, the program did indicate the correct value. However, when the probe was subsequently placed in a basic solution, it took time to get to the correct pH value (value associated with the basic solution). The response time of the pH sensor was slow. This caused a problem for turning the water pump on and off. The water pump turns automatically on at a low pH value (pH value <5.5). Because the response time of the pH sensor was slow when switching from a low pH value to a high pH value, slightly more water was pumped into the system.

6.	The flow sensor did not respond. As a result, we had to remove that flow sensor from our system.

7.	Due to the problems with the pH sensor, we omitted the humidity sensor due to lack of time. As a result, we have focused more on the pH sensor, the light intensity sensor and the temperature sensor.

8.	The ESP 32 board's USB connector was weak.

9.	While creating a suitable enclosure for the ESP 32 board, the USB cable had to be disconnected again, causing the USB to become loose. After this, we carefully glued it so that it could function properly again.


<provide tests performed, describe problems encountered and how they were solved (or not)>

## 3.5 Proof of work

![](https://snipboard.io/v1b6z4.jpg)
![](https://snipboard.io/MV60S3.jpg)
![](https://snipboard.io/TUBaLE.jpg)

## 3.6 Files & Code

[Link to code](https://gitlab.com/hackomation1/2020/hackers/mech-tech-students/-/blob/e6ab7f824910ef80927f828051f047b24a529ac5/docs/files/Final_code_1_.zip)

## 3.7 References & Credits

[How to use Blynk app](https://maker.pro/arduino/projects/how-to-remotely-control-an-arduino-with-the-blynk-app#:text=according%20to20Blynk's%20website%2C%20%E2%80%9CBlynk,simply%20dragging%20and%20dropping%20widgets.22>)